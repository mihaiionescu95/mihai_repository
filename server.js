const express = require('express'),
      bodyParser = require('body-parser'),
      Sequelize = require('sequelize');
      
const sequelize = new Sequelize('mydb','root','',{
  dialect : 'mysql',
  operatorsAliases: false,
  define : {
    timestamps : false
  }
});

const Company = sequelize.define('Company',{
  id: {
    type:Sequelize.INTEGER,
    allowNull:false,
    autoIncrement: true,
    primaryKey: true
  },
  name : {
    type:Sequelize.STRING,
    allowNull:true,
    validate : {
      len:[3,40]
    }
  },
  details: {
    type:Sequelize.TEXT,
    allowNull:true
  }
},{
  underscored : true
});


const Investor = sequelize.define('Investor',{
  id: {
    type:Sequelize.INTEGER,
    allowNull:false,
    primaryKey: true
  },
  name : {
    type : Sequelize.STRING,
    allowNull : true,
    validate : {
      len : [3,40]
    }
  },
  details : {
    type : Sequelize.TEXT,
    allowNull : true,
  }
},{
  underscored : true
});

const Relationships = sequelize.define('Relationships',{
  idCompany: {
    type:Sequelize.INTEGER,
    allowNull:false
  },
  idInvestor: {
    type:Sequelize.INTEGER,
    allowNull:false
  }
});

// Investor.hasMany(Relationships);
// Company.hasMany(Relationships);

Relationships.belongsToMany(Company, { through: 'idCompany' });
Relationships.belongsToMany(Investor, { through: 'idInvestor' });


const app = express();
var cors = require('cors');
app.use(cors()); 


app.use(bodyParser.json());

app.get('/create', (req, res, next) => {
  sequelize.sync({force : true})
    .then(() => res.status(201).send('created'))
    .catch((err) => next(err));
});

app.get('/Company', (req, res, next) => {
  Company.findAndCountAll()
    .then(result => {
    console.log(result.count);
    res.status(200).json(result.rows);
  })
    .catch((err) => next(err));
});

app.get('/countCompany', (req, res, next) => {
  Company.findAndCountAll()
    .then(result => {
    
    res.status(200).json(result.count);
  })
    .catch((err) => next(err));
});

app.post('/Company', (req, res, next) => {
  Company.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((err) => next(err));
});

app.get('/Company/:name', (req, res, next) => {
  Company.findOne({ where: { name: req.params.name } }).then(Company => {
      if (Company!=null){
        getOrganization(req.params.name,function(body){res.status(200).json(body)});
      }
      else{
        getOrganization(req.params.name,function(body){res.status(200).json(body)});
        
      }
    })
    .catch((err) => next(err))
})

app.put('/Company/:id', (req, res, next) => {
  Company.findById(req.params.id)
    .then((Company) => {
      if (Company){
        return Company.update(req.body, {fields : ['name', 'details']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

app.delete('/Company/:id', (req, res, next) => {
  Company.findById(req.params.id)
    .then((Company) => {
      if (Company){
        return Company.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('removed')
      }
    })
    .catch((err) => next(err))
})

app.get('/Investor', (req, res, next) => {
  Investor.findAll()
    .then((Investor) => res.status(200).json(Investor))
    .catch((err) => next(err))
})

app.get('/countInvestor', (req, res, next) => {
  Investor.findAndCountAll()
    .then(result => {
    
    res.status(200).json(result.count);
  })
    .catch((err) => next(err));
});

app.post('/Investor', (req, res, next) => {
  Investor.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((err) => next(err))
})


app.get('/Investor/:name', (req, res, next) => {
  Investor.findOne({where: {name:req.params.name}}).then((Investor) => {
      if (Investor!=null){
        getInvestor(req.params.name,function(body){res.status(200).json(body)});
      }
      else{
        getInvestor(req.params.name,function(body){res.status(200).json(body)});
      }
    })
    .catch((err) => next(err));
});

app.put('/Investor/:id', (req, res, next) => {
  Investor.findById(req.params.id)
    .then((Investor) => {
      if (Investor){
        return Investor.update(req.body, {fields : ['id','name', 'details']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

app.delete('/Investor/:id', (req, res, next) => {
  Investor.findById(req.params.id)
    .then((Investor) => {
      if (Investor){
        return Investor.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('removed')
      }
    })
    .catch((err) => next(err))
})

app.get('/Relationships', (req, res, next) => {
  Relationships.findAll()
    .then((Relationships) => res.status(200).json(Relationships))
    .catch((err) => next(err))
})

app.post('/Relationships', (req, res, next) => {
  Relationships.create(req.body)
    .then(() => res.status(201).send('created'))
    .catch((err) => next(err))
})

app.get('/Relationships/:id', (req, res, next) => {
  Relationships.findById(req.params.id) .then((Relationships) => {
      if (Relationships){
        res.status(200).json(Relationships)
      }
      else{
        res.status(404).send('not found')
      }
    })
    .catch((err) => next(err))
})

app.put('/Relationships/:id', (req, res, next) => {
  Relationships.findById(req.params.id)
    .then((Relationships) => {
      if (Relationships){
        return Relationships.update(req.body, {fields : ['idCompany','idInvestor']})
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('modified')
      }
    })
    .catch((err) => next(err))
})

app.delete('/Relationships/:id', (req, res, next) => {
  Relationships.findById(req.params.id)
    .then((Relationships) => {
      if (Relationships){
        return Relationships.destroy()
      }
      else{
        res.status(404).send('not found')
      }
    })
    .then(() => {
      if (!res.headersSent){
        res.status(201).send('removed')
      }
    })
    .catch((err) => next(err));
});

function getOrganization(nume,cb)
{
  var request = require("request");

var options = { method: 'GET',
  url: 'https://api.crunchbase.com/v3.1/odm-organizations',
  qs: { name: nume,
        user_key: 'afdf68c084e19b6eb5f0e0fe217dc8d1'} };

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  cb(body);
});
}
function getInvestor(nume,cb)
{
  var request = require("request");

var options = { method: 'GET',
  url: 'https://api.crunchbase.com/v3.1/odm-people',
  qs: { name: nume,
        user_key: 'afdf68c084e19b6eb5f0e0fe217dc8d1' } };

request(options, function (error, response, body) {
  if (error) throw new Error(error);
  cb(body);
});
}



app.use((err, req, res, next) => {
  console.warn(err);
  res.status(500).send('some error');
});

app.listen(8080);